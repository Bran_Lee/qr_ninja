<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="43"/>
        <source>Foreground</source>
        <translation>前景色</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="56"/>
        <source>Background</source>
        <translation>背景色</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="66"/>
        <source>Images(*.png *.bmp *.jpg *.jpeg)</source>
        <translation>图像(*.png *.bmp *.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="67"/>
        <location filename="../src/mainwindow.cpp" line="235"/>
        <source>Logo</source>
        <translation>Logo</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="76"/>
        <source>PNG Files (*.png);;BMP Files (*.bmp);; JPEG Files (*.jgp *.jpeg)</source>
        <translation>PNG文件 (*.png);;BMP文件 (*.bmp);; JPEG文件 (*.jgp *.jpeg)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <location filename="../src/mainwindow.cpp" line="244"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="226"/>
        <source>QR Ninja</source>
        <translation>QR Ninja</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="228"/>
        <source>Config</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="229"/>
        <source>Code Mode:</source>
        <translation>编码模式：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="230"/>
        <source>EC Level:</source>
        <translation>错误校正水平：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="231"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="232"/>
        <source>Output Size:</source>
        <translation>输出大小：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="233"/>
        <source>Foreground:</source>
        <translation>前景色：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="234"/>
        <source>Background:</source>
        <translation>背景色：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="236"/>
        <source>Browse...</source>
        <translation>浏览...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="238"/>
        <source>Data</source>
        <translation>数据</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="239"/>
        <source>Input data to be encoded...</source>
        <translation>输入待编码数据...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>Preview</source>
        <translation>预览</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="243"/>
        <source>Operation</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="245"/>
        <source>Generate</source>
        <translation>生成</translation>
    </message>
</context>
<context>
    <name>QRGenLevelModel</name>
    <message>
        <location filename="../src/core/qrgenlevelmodel.cpp" line="11"/>
        <source>L(Maximum of %7 Error Correction)</source>
        <translation>L（最大%7错误校正）</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenlevelmodel.cpp" line="12"/>
        <source>M(Maximum of %15 Error Correction)</source>
        <translation>M（最大%15错误校正）</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenlevelmodel.cpp" line="13"/>
        <source>Q(Maximum of %25 Error Correction)</source>
        <translation>Q（最大%25错误校正）</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenlevelmodel.cpp" line="14"/>
        <source>H(Maximum of %30 Error Correction)</source>
        <translation>H（最大%30错误校正）</translation>
    </message>
</context>
<context>
    <name>QRGenModeModel</name>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="11"/>
        <source>Numeric</source>
        <translation>数字</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="12"/>
        <source>Alphabet-numeric</source>
        <translation>字母数字</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="13"/>
        <source>8-bit</source>
        <translation>字节</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="14"/>
        <source>Kanji</source>
        <translation>日本汉字</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="15"/>
        <source>ECI</source>
        <translation>ECI</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="16"/>
        <source>FNC1-First Position</source>
        <translation>FNC1-1</translation>
    </message>
    <message>
        <location filename="../src/core/qrgenmodemodel.cpp" line="17"/>
        <source>FNC1-Second Position</source>
        <translation>FNC1-2</translation>
    </message>
</context>
</TS>
