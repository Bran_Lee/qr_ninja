#-------------------------------------------------
#
# Project created by QtCreator 2017-04-09T19:19:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QR_Ninja
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TRANSLATIONS += ./translations/app_zh_CN.ts
INCLUDEPATH += ./src/core

HEADERS += \
    src/mainwindow.h \
    src/core/qrgenerator.h \
    src/core/qrgeneratorimpl.h \
    src/core/qrgenlevelmodel.h \
    src/core/qrgenmodemodel.h \
    src/core/interface/qrgeneratorif.h \
    src/core/interface/qrgenmodelif.h \
    src/qrpreviewer.h

SOURCES += \
    src/main.cpp\
    src/mainwindow.cpp \
    src/core/qrgenerator.cpp \
    src/core/qrgeneratorimpl.cpp \
    src/core/qrgenlevelmodel.cpp \
    src/core/qrgenmodemodel.cpp \
    src/core/interface/qrgeneratorif.cpp \
    src/core/interface/qrgenmodelif.cpp \
    src/qrpreviewer.cpp

#libqrencode
DEFINES += HAVE_CONFIG_H
INCLUDEPATH += src/3rd/libqrencode-master
HEADERS  += \
    src/3rd/libqrencode-master/bitstream.h \
    src/3rd/libqrencode-master/config.h \
    src/3rd/libqrencode-master/mask.h \
    src/3rd/libqrencode-master/mmask.h \
    src/3rd/libqrencode-master/mqrspec.h \
    src/3rd/libqrencode-master/qrencode.h \
    src/3rd/libqrencode-master/qrencode_inner.h \
    src/3rd/libqrencode-master/qrinput.h \
    src/3rd/libqrencode-master/qrspec.h \
    src/3rd/libqrencode-master/rsecc.h \
    src/3rd/libqrencode-master/split.h
SOURCES +=  \
    src/3rd/libqrencode-master/mask.c \
    src/3rd/libqrencode-master/mmask.c \
    src/3rd/libqrencode-master/mqrspec.c \
    src/3rd/libqrencode-master/qrencode.c \
    src/3rd/libqrencode-master/qrinput.c \
    src/3rd/libqrencode-master/qrspec.c \
    src/3rd/libqrencode-master/rsecc.c \
    src/3rd/libqrencode-master/split.c \
    src/3rd/libqrencode-master/bitstream.c

RESOURCES += \
    resources.qrc

