#include <QGroupBox>
#include <QComboBox>
#include <QSpinBox>
#include <QTextEdit>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QPainter>
#include <QPixmap>
#include <QColorDialog>
#include <QFileDialog>
#include "qrgenerator.h"
#include "qrgenmodemodel.h"
#include "qrgenlevelmodel.h"
#include "qrpreviewer.h"
#include "mainwindow.h"

#define DEFAULT_FOREGROUND  "#ff000000"
#define DEFAULT_BACKGROUND  "#ffffffff"
#define BTN_COLOR_PROPERTY  "current_color"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
  initUI();
  initData();
  initConnections();
}

MainWindow::~MainWindow()
{

}

void MainWindow::onForeground()
{
  QString cur_color = foreground_btn_->property(BTN_COLOR_PROPERTY).toString();
  QString new_color = QColorDialog::getColor(QColor(cur_color),
                                             this,
                                             tr("Foreground"),
                                             QColorDialog::ShowAlphaChannel).name(QColor::HexArgb);

  foreground_btn_->setText(new_color);
  foreground_btn_->setIcon(createIcon(new_color));
  foreground_btn_->setProperty(BTN_COLOR_PROPERTY, new_color);
}

void MainWindow::onBackground()
{
  QString cur_color = background_btn_->property(BTN_COLOR_PROPERTY).toString();
  QString new_color = QColorDialog::getColor(QColor(cur_color),
                                             this,
                                             tr("Background"),
                                             QColorDialog::ShowAlphaChannel).name(QColor::HexArgb);

  background_btn_->setText(new_color);
  background_btn_->setIcon(createIcon(new_color));
  background_btn_->setProperty(BTN_COLOR_PROPERTY, new_color);
}

void MainWindow::onLogoBrowse()
{
  QString filter = tr("Images(*.png *.bmp *.jpg *.jpeg)");
  QString logo_file = QFileDialog::getOpenFileName(this, tr("Logo"), QString(), filter);
  if(logo_file.isEmpty())
    return;

  logo_path_edt_->setText(logo_file);
}

void MainWindow::onSave()
{
  QString filter = tr("PNG Files (*.png);;BMP Files (*.bmp);; JPEG Files (*.jgp *.jpeg)");
  QString save_file = QFileDialog::getSaveFileName(this, tr("Save"), QString(), filter);
  if(save_file.isEmpty())
    return;
  previewer_->saveImage(save_file);
}

void MainWindow::onGenerate()
{
  QRGenerator::QRInfo qrinfo;
  qrinfo.foreground = QColor(foreground_btn_->property(BTN_COLOR_PROPERTY).toString());
  qrinfo.background = QColor(background_btn_->property(BTN_COLOR_PROPERTY).toString());
  qrinfo.mode       = mode_cmb_->currentData(QRGenModelIF::ValueRole).toInt();
  qrinfo.eclevel    = eclevel_cmb_->currentData(QRGenModelIF::ValueRole).toInt();
  qrinfo.width      = output_size_spn_->value();
  qrinfo.version    = version_spn_->value();
  qrinfo.logo       = logo_path_edt_->text();
  qrinfo.data       = data_edt_->toPlainText().toLatin1();
  qrinfo.case_sensitive = true;

  previewer_->setImage(qrgenerator_->generate(qrinfo));
  save_btn_->setEnabled(!previewer_->isImageNull());
}

void MainWindow::initUI()
{
  resize(640, 320);

  // init instances
  config_grp_      = new QGroupBox(this);
  mode_lbl_        = new QLabel(this);
  mode_cmb_        = new QComboBox(this);
  eclevel_lbl_     = new QLabel(this);
  eclevel_cmb_     = new QComboBox(this);
  version_lbl_     = new QLabel(this);
  version_spn_     = new QSpinBox(this);
  output_size_lbl_ = new QLabel(this);
  output_size_spn_ = new QSpinBox(this);
  foreground_lbl_  = new QLabel(this);
  foreground_btn_  = new QPushButton(this);
  background_lbl_  = new QLabel(this);
  background_btn_  = new QPushButton(this);
  logo_grp_        = new QGroupBox(this);
  logo_path_edt_   = new QLineEdit(this);
  logo_browse_btn_ = new QPushButton(this);

  data_grp_ = new QGroupBox(this);
  data_edt_ = new QTextEdit(this);

  preview_grp_ = new QGroupBox(this);
  previewer_   = new QRPreviewer(this);

  operation_grp_ = new QGroupBox(this);
  save_btn_      = new QPushButton(this);
  generate_btn_  = new QPushButton(this);

  save_btn_->setEnabled(!previewer_->isImageNull());

  // init layout
  QHBoxLayout *logo_grp_layout = new QHBoxLayout();
  logo_grp_layout->addWidget(logo_path_edt_);
  logo_grp_layout->addWidget(logo_browse_btn_);
  logo_grp_->setLayout(logo_grp_layout);

  logo_path_edt_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

  QGridLayout *config_grp_layout = new QGridLayout();
  config_grp_layout->addWidget(mode_lbl_,       0, 0);
  config_grp_layout->addWidget(mode_cmb_,       0, 1);
  config_grp_layout->addWidget(eclevel_lbl_,    1, 0);
  config_grp_layout->addWidget(eclevel_cmb_,    1, 1);
  config_grp_layout->addWidget(output_size_lbl_,  2, 0);
  config_grp_layout->addWidget(output_size_spn_,  2, 1);
  config_grp_layout->addWidget(version_lbl_,    3, 0);
  config_grp_layout->addWidget(version_spn_,    3, 1);
  config_grp_layout->addWidget(foreground_lbl_, 4, 0);
  config_grp_layout->addWidget(foreground_btn_, 4, 1);
  config_grp_layout->addWidget(background_lbl_, 5, 0);
  config_grp_layout->addWidget(background_btn_, 5, 1);
  config_grp_layout->addWidget(logo_grp_,       6, 0, 1, 2);
  config_grp_layout->setColumnStretch(0, 0);
  config_grp_layout->setColumnStretch(1, 1);
  config_grp_->setLayout(config_grp_layout);

  mode_cmb_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  eclevel_cmb_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  output_size_spn_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  version_spn_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  foreground_btn_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
  background_btn_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

  QGridLayout *data_grp_layout = new QGridLayout();
  data_grp_layout->addWidget(data_edt_);
  data_grp_->setLayout(data_grp_layout);

  QGridLayout *preview_grp_layout = new QGridLayout();
  preview_grp_layout->addWidget(previewer_);
  preview_grp_->setLayout(preview_grp_layout);

  QHBoxLayout *operation_grp_layout = new QHBoxLayout();
  operation_grp_layout->addStretch();
  operation_grp_layout->addWidget(save_btn_);
  operation_grp_layout->addWidget(generate_btn_);
  operation_grp_->setLayout(operation_grp_layout);

  QVBoxLayout *main_left_layout = new QVBoxLayout();
  main_left_layout->addWidget(config_grp_);
  main_left_layout->addWidget(data_grp_);

  QVBoxLayout *main_right_layout = new QVBoxLayout();
  main_right_layout->addWidget(preview_grp_, 1);
  main_right_layout->addWidget(operation_grp_, 0);

  QHBoxLayout *main_layout = new QHBoxLayout();
  main_layout->addLayout(main_left_layout, 0);
  main_layout->addLayout(main_right_layout, 1);

  setCentralWidget(new QWidget(this));
  centralWidget()->setLayout(main_layout);

  // update texts
  UpdateTexts();
}

void MainWindow::initData()
{
  qrgenerator_ = new QRGenerator(this);

  mode_cmb_->setModel(qrgenerator_->modeModel());
  mode_cmb_->setCurrentIndex(qrgenerator_->modeModel()->defaultRow());

  eclevel_cmb_->setModel(qrgenerator_->eclevelModel());
  eclevel_cmb_->setCurrentIndex(qrgenerator_->eclevelModel()->defaultRow());

  version_spn_->setRange(qrgenerator_->minVersion(), qrgenerator_->maxVersion());
  version_spn_->setValue(qrgenerator_->defaultVersion());

  output_size_spn_->setRange(qrgenerator_->minOutputSize(), qrgenerator_->maxOutputSize());
  output_size_spn_->setValue(qrgenerator_->defaultOutputSize());

  foreground_btn_->setText(DEFAULT_FOREGROUND);
  foreground_btn_->setIcon(createIcon(DEFAULT_FOREGROUND));
  foreground_btn_->setProperty(BTN_COLOR_PROPERTY, QString(DEFAULT_FOREGROUND));

  background_btn_->setText(DEFAULT_BACKGROUND);
  background_btn_->setIcon(createIcon(DEFAULT_BACKGROUND));
  background_btn_->setProperty(BTN_COLOR_PROPERTY, QString(DEFAULT_BACKGROUND));
}

void MainWindow::initConnections()
{
  connect(foreground_btn_, SIGNAL(clicked(bool)), SLOT(onForeground()));
  connect(background_btn_, SIGNAL(clicked(bool)), SLOT(onBackground()));
  connect(logo_browse_btn_, SIGNAL(clicked(bool)), SLOT(onLogoBrowse()));
  connect(save_btn_, SIGNAL(clicked(bool)), SLOT(onSave()));
  connect(generate_btn_, SIGNAL(clicked(bool)), SLOT(onGenerate()));
}

void MainWindow::UpdateTexts()
{
  setWindowTitle(tr("QR Ninja"));

  config_grp_->setTitle(tr("Config"));
  mode_lbl_->setText(tr("Code Mode:"));
  eclevel_lbl_->setText(tr("EC Level:"));
  version_lbl_->setText(tr("Version:"));
  output_size_lbl_->setText(tr("Output Size:"));
  foreground_lbl_->setText(tr("Foreground:"));
  background_lbl_->setText(tr("Background:"));
  logo_grp_->setTitle(tr("Logo"));
  logo_browse_btn_->setText(tr("Browse..."));

  data_grp_->setTitle(tr("Data"));
  data_edt_->setPlaceholderText(tr("Input data to be encoded..."));

  preview_grp_->setTitle(tr("Preview"));

  operation_grp_->setTitle(tr("Operation"));
  save_btn_->setText(tr("Save"));
  generate_btn_->setText(tr("Generate"));
}

QIcon MainWindow::createIcon(const QString &color, const int size)
{
  QImage canvas(size, size, QImage::Format_ARGB32);
  QPainter painter(&canvas);
  painter.fillRect(QRect(0, 0, size, size), QColor(color));
  painter.end();

  return QIcon(QPixmap::fromImage(canvas));
}
