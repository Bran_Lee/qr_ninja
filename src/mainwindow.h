#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QGroupBox;
class QComboBox;
class QSpinBox;
class QTextEdit;
class QLabel;
class QLineEdit;
class QPushButton;
class QRGenerator;
class QRPreviewer;

class MainWindow : public QMainWindow
{
  Q_OBJECT
public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();

private slots:
  void onForeground();
  void onBackground();
  void onLogoBrowse();
  void onSave();
  void onGenerate();

private:
  void initUI();
  void initData();
  void initConnections();
  void UpdateTexts();

  QIcon createIcon(const QString &color, const int size = 256);

private:
  // ui
  QGroupBox   *config_grp_;
  QLabel      *mode_lbl_;
  QComboBox   *mode_cmb_;
  QLabel      *eclevel_lbl_;
  QComboBox   *eclevel_cmb_;
  QLabel      *version_lbl_;
  QSpinBox    *version_spn_;
  QLabel      *output_size_lbl_;
  QSpinBox    *output_size_spn_;
  QLabel      *foreground_lbl_;
  QPushButton *foreground_btn_;
  QLabel      *background_lbl_;
  QPushButton *background_btn_;
  QGroupBox   *logo_grp_;
  QLineEdit   *logo_path_edt_;
  QPushButton *logo_browse_btn_;

  QGroupBox   *data_grp_;
  QTextEdit   *data_edt_;

  QGroupBox   *preview_grp_;
  QRPreviewer *previewer_;

  QGroupBox   *operation_grp_;
  QPushButton *save_btn_;
  QPushButton *generate_btn_;

  // data
  QImage       qrcode_;
  QRGenerator *qrgenerator_;
};

#endif // MAINWINDOW_H
