#ifndef QRPREVIEWER_H
#define QRPREVIEWER_H

#include <QWidget>

class QRPreviewer : public QWidget
{
  Q_OBJECT
public:
  explicit QRPreviewer(QWidget *parent = 0);

  bool isImageNull() const;
  void setImage(const QImage &image);
  void saveImage(const QString &path);

protected:
  void paintEvent(QPaintEvent *event);

private:
  QImage qrcode_;
};

#endif // QRPREVIEWER_H
