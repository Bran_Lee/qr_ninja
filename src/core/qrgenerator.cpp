#include "qrgenerator.h"
#include "qrgeneratorimpl.h"

QRGenerator::QRGenerator(QObject *parent) : QRGeneratorIF(parent)
{
  _impl = new QRGeneratorImpl(this);
}

QRGenModelIF* QRGenerator::modeModel()
{
  return _impl->modeModel();
}

QRGenModelIF* QRGenerator::eclevelModel()
{
  return _impl->eclevelModel();
}

int QRGenerator::minVersion() const
{
  return _impl->minVersion();
}

int QRGenerator::maxVersion() const
{
  return _impl->maxVersion();
}

int QRGenerator::QRGenerator::minOutputSize() const
{
  return _impl->minOutputSize();
}

int QRGenerator::QRGenerator::maxOutputSize() const
{
  return _impl->maxOutputSize();
}

int QRGenerator::defaultModeIndex() const
{
   return _impl->defaultModeIndex();
}

int QRGenerator::defaultECLevelIndex() const
{
  return _impl->defaultECLevelIndex();
}

int QRGenerator::defaultVersion() const
{
  return _impl->defaultVersion();
}

int QRGenerator::defaultOutputSize() const
{
  return _impl->defaultOutputSize();
}

QImage QRGenerator::generate(const QRInfo &info)
{
  return _impl->generate(info);
}

