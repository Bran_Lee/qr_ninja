#ifndef QRGENERATOR_H
#define QRGENERATOR_H

#include "./interface/qrgeneratorif.h"

class QRGenerator : public QRGeneratorIF
{
  Q_OBJECT
public:
  QRGenerator(QObject *parent=Q_NULLPTR);

  // properties
  Q_PROPERTY(QAbstractListModel* modeModel     READ modeModel)
  Q_PROPERTY(QAbstractListModel* eclevelModel  READ eclevelModel)

  Q_PROPERTY(int minVersion    READ minVersion)
  Q_PROPERTY(int maxVersion    READ maxVersion)
  Q_PROPERTY(int minOutputSize READ minOutputSize)
  Q_PROPERTY(int maxOutputSize READ maxOutputSize)

  Q_PROPERTY(int defaultModeIndex    READ defaultModeIndex)
  Q_PROPERTY(int defaultECLevelIndex READ defaultECLevelIndex)
  Q_PROPERTY(int defaultVersion      READ defaultVersion)
  Q_PROPERTY(int defaultOutputSize   READ defaultOutputSize)

  // inherited functions
  Q_INVOKABLE virtual QRGenModelIF *modeModel();
  Q_INVOKABLE virtual QRGenModelIF *eclevelModel();
  Q_INVOKABLE virtual int minVersion() const;
  Q_INVOKABLE virtual int maxVersion() const;
  Q_INVOKABLE virtual int minOutputSize() const;
  Q_INVOKABLE virtual int maxOutputSize() const;

  Q_INVOKABLE virtual int defaultModeIndex() const;
  Q_INVOKABLE virtual int defaultECLevelIndex() const;
  Q_INVOKABLE virtual int defaultVersion() const;
  Q_INVOKABLE virtual int defaultOutputSize() const;

  Q_INVOKABLE virtual QImage generate(const QRInfo &info);

private:
  QRGeneratorIF *_impl;
};

#endif // QRGENERATOR_H
