#include "qrgenlevelmodel.h"
#include "qrencode.h"

QRGenLevelModel::QRGenLevelModel(QObject *parent) : QRGenModelIF(parent)
{
  // init role names
  _role_names.insert(KeyRole, QByteArray("key"));
  _role_names.insert(ValueRole, QByteArray("value"));

  // init data
  _key_value_list.append(KeyValuePair(tr("L(Maximum of %7 Error Correction)"), QR_ECLEVEL_L));  // default row
  _key_value_list.append(KeyValuePair(tr("M(Maximum of %15 Error Correction)"), QR_ECLEVEL_M));
  _key_value_list.append(KeyValuePair(tr("Q(Maximum of %25 Error Correction)"), QR_ECLEVEL_Q));
  _key_value_list.append(KeyValuePair(tr("H(Maximum of %30 Error Correction)"), QR_ECLEVEL_H));

  _default_row = 0;
}

int QRGenLevelModel::defaultRow() const
{
  return _default_row;
}
