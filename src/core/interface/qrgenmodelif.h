#ifndef QRGENMODELIF_H
#define QRGENMODELIF_H

#include <QAbstractListModel>

class QRGenModelIF : public QAbstractListModel
{
  Q_OBJECT
public:
  QRGenModelIF(QObject *parent=Q_NULLPTR);

  // inherited functions
  Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
  Q_INVOKABLE virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  Q_INVOKABLE virtual QHash<int,QByteArray> roleNames() const;

  // new functions
  Q_INVOKABLE virtual QVariant data(int row, int role) const;
  Q_INVOKABLE virtual QVariant data(int row, const QString &roleName) const;
  Q_INVOKABLE virtual int defaultRow() const = 0;

  enum Roles
  {
    KeyRole   = Qt::UserRole + 1,
    ValueRole = Qt::UserRole + 2,
  };

protected:
  typedef struct KeyValuePair_
  {
    QString key;
    int     value;

    KeyValuePair_(const QString &key_ = QString(),
                  const int value_ = -1)
    {
      this->key   = key_;
      this->value = value_;
    }
  }KeyValuePair;

  // implements should fill in following data
  QHash<int,QByteArray> _role_names;
  QList<KeyValuePair>   _key_value_list;
};

#endif // QRGENMODELIF_H
