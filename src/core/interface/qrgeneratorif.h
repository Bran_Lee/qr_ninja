#ifndef QRGENERATORIF_H
#define QRGENERATORIF_H

#include <QObject>
#include <QColor>
#include <QImage>
#include "qrgenmodelif.h"

// interface
class QRGeneratorIF : public QObject
{
  Q_OBJECT
public:
  QRGeneratorIF(QObject *parent=Q_NULLPTR);

  // model of encoding mode
  virtual QRGenModelIF* modeModel() = 0;
  // model of error correction level
  virtual QRGenModelIF* eclevelModel() = 0;
  // vertion
  virtual int minVersion() const = 0;
  virtual int maxVersion() const = 0;
  // output size
  virtual int minOutputSize() const = 0;
  virtual int maxOutputSize() const = 0;
  // default values
  virtual int defaultModeIndex() const = 0;
  virtual int defaultECLevelIndex() const = 0;
  virtual int defaultVersion() const = 0;
  virtual int defaultOutputSize() const = 0;

  // generation info
  typedef struct
  {
    QByteArray data;           // data to be encoded
    int        width;          // image size, in pixel
    int        version;        // version
    int        eclevel;        // error correction level
    int        mode;           // encoding mode
    bool       case_sensitive; // case sensitive

    QString    logo;       // logo file
    QColor     foreground; // forground color
    QColor     background; // background color
  }QRInfo;

  // generate QR code
  virtual QImage generate(const QRInfo &info) = 0;
};

#endif // QRGENERATORIF_H
