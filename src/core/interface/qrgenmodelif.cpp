#include "qrgenmodelif.h"

QRGenModelIF::QRGenModelIF(QObject *parent) : QAbstractListModel(parent)
{
}

int QRGenModelIF::rowCount(const QModelIndex &parent/* = QModelIndex()*/) const
{
  Q_UNUSED(parent);
  return _key_value_list.size();
}

QVariant QRGenModelIF::data(const QModelIndex &index, int role/* = Qt::DisplayRole*/) const
{
  return data(index.row(), role);
}

QVariant QRGenModelIF::data(int row, int role) const
{
  if(row < 0 || row >= _key_value_list.size())
    return QVariant();

  switch(role)
  {
    case Qt::DisplayRole:
    case KeyRole:
      return _key_value_list.at(row).key;
    case ValueRole:
      return _key_value_list.at(row).value;
    default:
      return QVariant();
  }
}

QVariant QRGenModelIF::data(int row, const QString &roleName) const
{
  int role = _role_names.key(roleName.toLatin1(), -1);
  return data(row, role);
}

QHash<int,QByteArray> QRGenModelIF::roleNames() const
{
  return _role_names;
}
