#include <QPainter>
#include "qrgeneratorimpl.h"
#include "qrgenmodemodel.h"
#include "qrgenlevelmodel.h"
#include "qrencode.h"

QRGeneratorImpl::QRGeneratorImpl(QObject *parent) :
  QRGeneratorIF(parent),
  _min_version(0), // auto
  _max_version(QRSPEC_VERSION_MAX),
  _min_output_size(64),
  _max_output_size(2048),
  _default_version(0), // auto
  _default_output_size(qMax(QRSPEC_VERSION_MAX, 512))
{
  _mode_model    = new QRGenModeModel(this);
  _eclevel_model = new QRGenLevelModel(this);
}

QRGenModelIF* QRGeneratorImpl::modeModel()
{
  return _mode_model;
}

QRGenModelIF *QRGeneratorImpl::eclevelModel()
{
  return _eclevel_model;
}

int QRGeneratorImpl::minVersion() const
{
  return _min_version;
}

int QRGeneratorImpl::maxVersion() const
{
  return _max_version;
}

int QRGeneratorImpl::minOutputSize() const
{
  return _min_output_size;
}

int QRGeneratorImpl::maxOutputSize() const
{
  return _max_output_size;
}

int QRGeneratorImpl::defaultModeIndex() const
{
  return qobject_cast<QRGenModelIF*>(_mode_model)->defaultRow();
}

int QRGeneratorImpl::defaultECLevelIndex() const
{
  return qobject_cast<QRGenModelIF*>(_eclevel_model)->defaultRow();
}

int QRGeneratorImpl::defaultVersion() const
{
  return _default_version;
}

int QRGeneratorImpl::defaultOutputSize() const
{
  return _default_output_size;
}

QImage QRGeneratorImpl::generate(const QRInfo &info)
{
  // encoding
  QRcode *qrcode = QRcode_encodeString(info.data.constData(),
                                       info.version,
                                       (QRecLevel)info.eclevel,
                                       (QRencodeMode)info.mode,
                                       info.case_sensitive);
  if(qrcode == Q_NULLPTR)
    return QImage();

  // init image
  int image_size = info.width;
  QImage output_image(image_size, image_size, QImage::Format_ARGB32);

  // init painter
  int cell_logic_size = 10;
  int image_logic_size = cell_logic_size * qrcode->width;
  QPainter painter(&output_image);
  painter.setViewport(0, 0, image_size, image_size);
  painter.setWindow(0, 0, image_logic_size, image_logic_size);

  // paint background
  QColor background = info.background;
  if(!background.isValid())
    background = QColor(Qt::transparent);
  painter.fillRect(QRect(0, 0, image_logic_size, image_logic_size), background);

  // paint forground
  QColor foreground = info.foreground;
  if(!foreground.isValid())
    foreground = QColor(Qt::black);
  for(int i=0; i<qrcode->width; ++i)
  {
    for(int j=0; j<qrcode->width; ++j)
    {
      // white cell
      int offset = i * qrcode->width + j;
      if(!(qrcode->data[offset] & 1))
        continue;

      // black cell
      QRect cell_rect = QRect(i*cell_logic_size,
                              j*cell_logic_size,
                              cell_logic_size,
                              cell_logic_size);
      painter.fillRect(cell_rect, foreground);
    }
  }
  QRcode_free(qrcode);
  qrcode = Q_NULLPTR;

  // paint logo
  if(!info.logo.isEmpty())
  {
    QImage logo_image;
    if(logo_image.load(info.logo))
    {
      double logo_size = image_logic_size * 0.2;
      double logo_offset = (image_logic_size - logo_size) / 2;
      QRectF logo_rect = QRect(logo_offset,
                               logo_offset,
                               logo_size,
                               logo_size);
      painter.drawImage(logo_rect, logo_image);
    }
  }

  return output_image;
}
