#include "qrgenmodemodel.h"
#include "qrencode.h"

QRGenModeModel::QRGenModeModel(QObject *parent) : QRGenModelIF(parent)
{
  // init role names
  _role_names.insert(KeyRole, QByteArray("key"));
  _role_names.insert(ValueRole, QByteArray("value"));

  // init data
  _key_value_list.append(KeyValuePair(tr("Numeric"), QR_MODE_NUM));
  _key_value_list.append(KeyValuePair(tr("Alphabet-numeric"), QR_MODE_AN));
  _key_value_list.append(KeyValuePair(tr("8-bit"), QR_MODE_8));     // default row
  _key_value_list.append(KeyValuePair(tr("Kanji"), QR_MODE_KANJI));
  _key_value_list.append(KeyValuePair(tr("ECI"), QR_MODE_ECI));
  _key_value_list.append(KeyValuePair(tr("FNC1-First Position"), QR_MODE_FNC1FIRST));
  _key_value_list.append(KeyValuePair(tr("FNC1-Second Position"), QR_MODE_FNC1SECOND));

  _default_row = 2;
}

int QRGenModeModel::defaultRow() const
{
  return _default_row;
}
