#ifndef QRGENMODEMODEL_H
#define QRGENMODEMODEL_H

#include "./interface/qrgenmodelif.h"

class QRGenModeModel : public QRGenModelIF
{
  Q_OBJECT
public:
  QRGenModeModel(QObject *parent=Q_NULLPTR);

  Q_INVOKABLE virtual int defaultRow() const;

private:
  int _default_row;
};

#endif // QRGENMODEMODEL_H
