#ifndef QRGENLEVELMODEL_H
#define QRGENLEVELMODEL_H

#include "./interface/qrgenmodelif.h"

class QRGenLevelModel : public QRGenModelIF
{
  Q_OBJECT
public:
  QRGenLevelModel(QObject *parent=Q_NULLPTR);

  Q_INVOKABLE virtual int defaultRow() const;

private:
  int _default_row;
};

#endif // QRGENLEVELMODEL_H
