#ifndef QRGENERATORIMPL_H
#define QRGENERATORIMPL_H

#include "./interface/qrgeneratorif.h"

class QRGeneratorImpl : public QRGeneratorIF
{
  Q_OBJECT
public:
  QRGeneratorImpl(QObject *parent=Q_NULLPTR);

  // inherited functions
  QRGenModelIF* modeModel();
  QRGenModelIF* eclevelModel();
  int minVersion() const;
  int maxVersion() const;
  int minOutputSize() const;
  int maxOutputSize() const;

  int defaultModeIndex() const;
  int defaultECLevelIndex() const;
  int defaultVersion() const;
  int defaultOutputSize() const;

  QImage generate(const QRInfo &info);

private:
  const int _min_version;
  const int _max_version;
  const int _min_output_size;
  const int _max_output_size;
  const int _default_version;
  const int _default_output_size;

  QRGenModelIF *_mode_model;
  QRGenModelIF *_eclevel_model;
};

#endif // QRGENERATORIMPL_H
