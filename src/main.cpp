#include <QApplication>
#include <QTranslator>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
  // create app
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QApplication app(argc, argv);

  // init translators
  QTranslator sysTranslator;
  sysTranslator.load(":/translations/qt_zh_CN.qm");
  app.installTranslator(&sysTranslator);

  QTranslator appTranslator;
  appTranslator.load(":/translations/app_zh_CN.qm");
  app.installTranslator(&appTranslator);

  // init window
  MainWindow win;
  win.show();

  return app.exec();
}
