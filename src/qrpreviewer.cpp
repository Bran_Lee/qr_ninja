#include <QPainter>
#include "qrpreviewer.h"

QRPreviewer::QRPreviewer(QWidget *parent) : QWidget(parent)
{
}

bool QRPreviewer::isImageNull() const
{
  return qrcode_.isNull();
}

void QRPreviewer::setImage(const QImage &image)
{
  qrcode_ = image;
  repaint();
}

void QRPreviewer::saveImage(const QString &path)
{
  if(path.isEmpty() || isImageNull())
    return;
  qrcode_.save(path);
}

void QRPreviewer::paintEvent(QPaintEvent *event)
{
  if(qrcode_.isNull())
    return;

  int widget_width  = width();
  int widget_height = height();
  int code_width    = qrcode_.width();
  int code_height   = qrcode_.height();
  int scaled_width  = code_width * widget_height / code_height;
  int scaled_height = code_height * widget_width / code_width;

  QPainter painter(this);
  if(scaled_width <= widget_width)
  {
    int offset = widget_width - scaled_width;
    painter.drawImage(QRect(offset/2, 0, widget_width - offset, widget_height), qrcode_);
  }
  else
  {
    int offset = widget_height - scaled_height;
    painter.drawImage(QRect(0, offset/2, widget_width, widget_height - offset), qrcode_);
  }
}
